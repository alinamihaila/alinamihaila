## Introduction

🌻 Hello, my name is Alina Mihaila.

I joined GitLab in February, 2020.

I am a Senior Backend Engineer in the [Engineering Productivity](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/) group.

Previously I was part of [Product Intelligence](https://about.gitlab.com/handbook/engineering/development/analytics/product-intelligence/) for about 2 years and a half.    

I took an Acting Manager role for Product Inteligence team for about 7 months. 

## Work style

I work mainly with the GitLab To-Do list. Mention me in a merge request or issue.

My calendar is up to date, if you want to have a chat.

## My day

Timezone: UTC+2.

6:30 I start my day by going to the gym and after having breakfast.

I prefer to have focus time in the mornings, it is in general more quiet.

12:30  We have lunch with my husband and sometimes we have a walk.

In the afternoon continue with work and meetings.

17:00-18:00 Ussualy end of the working day, sometimes I attend to later meetings.

On Fridays afternoon I look to take some time for learning, have training read a book.

## Fun facts

🎶 I started to learn piano in 2022, I I enjoy this a lot. I'm excited each day to take time to practice. I've learned I can get a deep focust to do this, and apply this learning in everything I'm doing.

📚 I like to read, and learn new things, I'd say I'm a curious person. Here is my [Goodreads profle](https://www.goodreads.com/user/show/64073363-alina-mihaila).

🙈 I do not like to be late, it's something that makes me go to the airport 3 hours.


